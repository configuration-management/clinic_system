package com.example.springboot.controller;
import com.example.springboot.common.Result;
import com.example.springboot.entity.Account;
import com.example.springboot.entity.User;
import com.example.springboot.service.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:7070")
@RestController
@RequestMapping("/loginController")
public class loginController {
    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public Result login(@RequestBody Map<String, String> loginRequest) {
        String username = loginRequest.get("username");
        String password = loginRequest.get("password");


        List<User> user = userService.findByUsername(username);
        User user1 = user.get(0);

        System.out.println(user1.getPassword());
        if (user != null && user1.getPassword().equals(password)) {
            // 登录成功，返回用户信息
            Map<String, Object> data = new HashMap<>();
            data.put("id", user1.getId());
            data.put("username", user1.getUsername());
            data.put("name", user1.getName());
            data.put("role", user1.getRole());
            System.out.println(Result.success(data) );
            return Result.success(data) ;
        } else {
            // 登录失败，返回错误信息
            return Result.error("401", "用户名或密码错误");
        }
        //对象{"role":"管理员","name":"管理员","id":1,"username":"admin"}

//        if (user != null && user1.getPassword().equals(password)) {
//            // 登录成功，返回用户信息
//            System.out.println(Result.success(user) );
//            return Result.success(user) ;
//        } else {
//            // 登录失败，返回错误信息
//            return Result.error("401", "用户名或密码错误");
//        }
//        //数组[{"id":1,"username":"admin","password":"1234","name":"管理员","role":"管理员"}]

    }

}

