package com.example.springboot.service;

import com.example.springboot.entity.Finance;
import com.example.springboot.mapper.FinanceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FinanceService {

    @Autowired
    private FinanceMapper financeMapper;

    public List<Finance> getAllFinances() {
        return financeMapper.getAllFinances();
    }

    public List<Finance> getFinanceByid(Long id) {
        return financeMapper.getFinanceByid(id);
    }

    public List<Finance> getFinanceByname(String name) {
        return financeMapper.getFinanceByname(name);
    }

    public List<Finance> getFinanceByamount(int amount) {
        return financeMapper.getFinanceByamount(amount);
    }

    public List<Finance> getFinanceBytype(String type) {
        return financeMapper.getFinanceBytype(type);
    }

    public void addFinance(Finance finance) {
        financeMapper.addFinance(finance);
    }

    public void editFinance(Finance finance) {
        financeMapper.editFinance(finance);
    }

    public void updateFinance(Finance finance) {
        financeMapper.updateFinance(finance);
    }

    public void deleteFinance(int id) {
        financeMapper.deleteFinance(id);
    }


    public List<Finance> getFinancesByMonth(Integer year, Integer month) {
        return financeMapper.getFinancesByMonth(year, month);
    }

    public List<Finance> getFinanceByidAndMonth(Long id, Integer year, Integer month) {
        return financeMapper.getFinancesByidAndMonth(id, year, month);
    }

    public List<Finance> getFinanceBynameAndMonth(String name, Integer year, Integer month) {
        return financeMapper.getFinancesBynameAndMonth(name, year, month);
    }

    public List<Finance> getFinanceByamountAndMonth(String amount, Integer year, Integer month) {
        return financeMapper.getFinancesByamountAndMonth(amount, year, month);
    }

    public List<Finance> getFinanceBytypeAndMonth(String type, Integer year, Integer month) {
        return financeMapper.getFinancesBytypeAndMonth(type, year, month);
    }

    public List<Finance> getFinanceBydateAndMonth(String date, Integer year, Integer month) {
        return financeMapper.getFinancesBydateAndMonth(date, year, month);
    }


    public List<Finance> getFinanceBydate(String date) {
        return financeMapper.getFinanceBydate(date);
    }



}
