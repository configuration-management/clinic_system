package com.example.springboot.service;

import com.example.springboot.entity.Inventory;
import com.example.springboot.entity.Treatment;
import com.example.springboot.mapper.TreatmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TreatmentService {

    @Autowired
    private TreatmentMapper treatmentMapper;

    public List<Treatment> selectAll() {
        return treatmentMapper.selectAll();
    }

    public List<Treatment> selectById(Long id) {
        return treatmentMapper.selectById(id);
    }

    public List<Treatment> selectByName(String name) {
        return treatmentMapper.selectByName(name);
    }

    public void addTreatment(Treatment treatment) {
        treatmentMapper.addTreatment(treatment);
    }

    public void editTreatment(Treatment treatment) {
        treatmentMapper.editTreatment(treatment);
    }

    public void deleteTreatment(Long id) {
        treatmentMapper.deleteTreatment(id);
    }
}
