package com.example.springboot.service;

import com.example.springboot.entity.Account;
import com.example.springboot.mapper.AccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {

    @Autowired
    private AccountMapper accountMapper;

    public List<Account> searchAccounts() {
        return accountMapper.searchAccounts();
    }

    public void addAccount(Account account) {
        accountMapper.addAccount(account);
    }

    public void updateAccount(Account account) {
        accountMapper.updateAccount(account);
    }

    public void deleteAccount(int id) {
        accountMapper.deleteAccount(id);
    }

    public List<Account> selectById(Long id) {
        return accountMapper.selectById(id);
    }

    public void deleteAccountBatch(List<Long> ids) {
        accountMapper.deleteAccountBatch(ids);
    }

    public List<Account> selectByName(String name) {
        return accountMapper.selectByName(name);
    }
}
