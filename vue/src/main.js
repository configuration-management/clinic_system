import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue'
import router from './router'


Vue.config.productionTip = false
Vue.use(ElementUI);

Vue.filter('formatDate', function(value) {
  if (!value) return ''
  return new Date(value).toISOString().slice(0, 10)
})//过滤器

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
