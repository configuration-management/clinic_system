package com.example.springboot.controller;

import com.example.springboot.common.Result;
import com.example.springboot.entity.Treatment;
import com.example.springboot.service.TreatmentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:7070")
@RestController
@RequestMapping("/treatments")
public class TreatmentController {

    @Autowired
    private TreatmentService treatmentService;

//    @GetMapping("/selectAll")
//    public Result getAllTreatments() {
//        List<Treatment> treatments = treatmentService.getAllTreatments();
//        return Result.success(treatments);
//    }
    /**
     * 添加分页功能PageHelper
     */
    @GetMapping("/selectAll")
    public Result selectAll(@RequestParam int page, @RequestParam int size) {
        PageHelper.startPage(page, size);
        List<Treatment> treatmentList = treatmentService.selectAll();
        PageInfo<Treatment> pageInfo = new PageInfo<>(treatmentList);
        return Result.success(pageInfo);
    }

    @GetMapping("/selectByid")
    public Result selectById(@RequestParam Long id,@RequestParam int page, @RequestParam int size) {
        PageHelper.startPage(page, size);
        List<Treatment> treatmentList = treatmentService.selectById(id);
        PageInfo<Treatment> pageInfo = new PageInfo<>(treatmentList);
        return Result.success(pageInfo);
    }

    @GetMapping("/selectByname")
    public Result selectByName(@RequestParam String name,@RequestParam int page, @RequestParam int size) {
        PageHelper.startPage(page, size);
        List<Treatment> treatmentList = treatmentService.selectByName(name);
        PageInfo<Treatment> pageInfo = new PageInfo<>(treatmentList);
        return Result.success(pageInfo);
    }

    @PostMapping("/add")
    public Result addTreatment(@RequestBody Treatment treatment) {
        treatmentService.addTreatment(treatment);
        return Result.success("新增成功");
    }

    @PutMapping("/edit")
    public Result editTreatment(@RequestBody Treatment treatment) {
        treatmentService.editTreatment(treatment);
        return Result.success("编辑成功");
    }

    @DeleteMapping("/delete/{id}")
    public Result deleteTreatment(@PathVariable Long id) {
        treatmentService.deleteTreatment(id);
        return Result.success("删除成功");
    }
}
