package com.example.springboot.mapper;

import com.example.springboot.entity.Patient;
import org.apache.ibatis.annotations.*;


import java.util.List;

@Mapper
public interface PatientMapper {

    @Select("select * from `patients` ")
    List<Patient> selectAll();

    @Select("select * from patients where id = #{id}")
    List<Patient> selectById(Long id);

    @Select("select * from patients where name = #{name}")
    List<Patient> selectByName(String name);

    @Insert("INSERT INTO patients (name, gender,date_of_birth, phone,email,address) VALUES (#{name}, #{gender},#{date_of_birth}, #{phone},#{email}, #{address})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void addPatient(Patient patient);

    @Update("UPDATE patients SET name = #{name}, gender = #{gender}, date_of_birth = #{date_of_birth},phone=#{phone}, email = #{email},address=#{address} WHERE id = #{id}")
    void updatePatient(Patient patient);

    @Delete("DELETE FROM patients WHERE id = #{id}")
    void deletePatient(@Param("id") int id);

    @Delete({
            "<script>",
            "DELETE FROM patients WHERE id IN ",
            "<foreach item='id' collection='list' open='(' separator=',' close=')'>#{id}</foreach>",
            "</script>"
    })
    void deletePatientBatch(List<Long> ids);

}
