import Vue from 'vue'
import VueRouter from 'vue-router'
import Manager from '../views/Manager.vue'
import InventoryView from "@/views/Manager/InventoryView.vue";
import InventoryView2 from "@/views/Manager/InventoryView2.vue";
import AppointmentView from "@/views/Manager/AppointmentView.vue";
import TreatmentView from "@/views/Manager/TreatmentView.vue";
import TreatmentView2 from "@/views/Manager/TreatmentView2.vue";
import AppointmentView2 from "@/views/Manager/AppointmentView2.vue";
import FinancialView from "@/views/Manager/FinancialView.vue";
import DoctorView from "@/views/Manager/DoctorView.vue";
import AccountView from "@/views/Manager/AccountView.vue";
import FinancialView2 from "@/views/Manager/FinancialView2.vue";

import HomeView from "@/views/Manager/HomeView.vue";
import PersonView from "@/views/Manager/PersonView.vue";
import login1 from "@/views/login1.vue";
import register from "@/views/register.vue";

import PatientView from "@/views/Manager/PatientView.vue";
import InventoryView3 from "@/views/Manager/InventoryView3.vue";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Manager',
    component: Manager,
    redirect:'/login1',//重定向到login1
    children:[      //子路由
      { path: 'home', name: 'home', component: HomeView},
      { path: 'inventory', name: 'InventoryView', component: InventoryView},
      { path: 'Inventory2', name: 'InventoryView2', component: InventoryView2},
      { path: 'Inventory3', name: 'InventoryView3', component: InventoryView3},
      { path: 'Appointment', name: 'AppointmentView',component: AppointmentView},
      { path: 'Appointment2',name: 'AppointmentView2',component: AppointmentView2},
      { path: 'Treatment',name: 'TreatmentView',component: TreatmentView},
      { path: 'Treatment2',name: 'TreatmentView2',component: TreatmentView2},
      { path: 'Financial', name: 'FinancialView',component: FinancialView},
      { path: 'Financial2',  name: 'FinancialView2',component: FinancialView2},
      { path: 'Doctor', name: 'DoctorView', component: DoctorView},
      { path: 'Account',name: 'AccountView',component: AccountView},
      { path: 'Person',name: 'PersonView',component: PersonView},
      { path: 'Patient',name: 'PatientView',component: PatientView},
      // { path: 'login',name: 'login',component: login1},
    ]
  },
    {
    path: '/login1',
    name: 'login1',
    component: login1
  },
  {
    path: '/register',
    name: 'register',
    component: register
  }


  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // }
]

const router = new VueRouter({
  // mode: 'history',
  // base: process.env.BASE_URL,
  base:'/CSvue',
  routes
})

export default router
