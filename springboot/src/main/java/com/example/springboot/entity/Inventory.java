package com.example.springboot.entity;

import lombok.Data;

import java.sql.Date;

//lombok简化get和set方法
@Data
public class Inventory {
    private int id;//序号
//    private String product_image; //产品图片
    private String product_name;  //产品名称
    private String category;      //类别
    private String brand;         //品牌
    private String supplier;      //供应商
    private String specification; //规格
    private String unit;          //单位
    private int quantity;         //数量
    private int selectedQuantity; // 新增字段用于保存所选数量
    private String price;         //价格
    private String product_batch; //产品批次
//    private Date expiry_date;     //产品有效期
//    private String status;        //状态


}
