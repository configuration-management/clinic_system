package com.example.springboot.controller;

import com.example.springboot.common.Result;
import com.example.springboot.entity.Account;
import com.example.springboot.service.AccountService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:7070")
@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;

//    @GetMapping("/search")
//    public Result searchAccounts(@RequestParam(required = false) String username) {
//        List<Account> accounts = accountService.searchAccounts(username);
//        return Result.success(accounts);
//    }

    @GetMapping("/search")
    public Result searchAccounts(@RequestParam int page, @RequestParam int size) {
        PageHelper.startPage(page, size);
        List<Account> accounts = accountService.searchAccounts();
        PageInfo<Account> pageInfo = new PageInfo<>(accounts);
        return Result.success(pageInfo);
    }

//    @GetMapping("/selectAll")
//    public Result selectAll(@RequestParam int page, @RequestParam int size) {
//        PageHelper.startPage(page, size);
//        List<Treatment> treatmentList = treatmentService.selectAll();
//        PageInfo<Treatment> pageInfo = new PageInfo<>(treatmentList);
//        return Result.success(pageInfo);
//    }

    @GetMapping("/selectByid")
    public Result selectById(@RequestParam Long id,@RequestParam int page, @RequestParam int size) {
        PageHelper.startPage(page, size);
        List<Account> accounts = accountService.selectById(id);
        PageInfo<Account> pageInfo = new PageInfo<>(accounts);
        return Result.success(pageInfo);
    }


    @GetMapping("/selectByname")
    public Result selectByName(@RequestParam String name,@RequestParam int page, @RequestParam int size) {
        PageHelper.startPage(page, size);
        List<Account> accounts = accountService.selectByName(name);
        PageInfo<Account> pageInfo = new PageInfo<>(accounts);
        return Result.success(pageInfo);
    }

    @PostMapping("/add")
    public Result addAccount(@RequestBody Account account) {
        accountService.addAccount(account);
        return Result.success("新增成功");
    }

    @PutMapping("/edit")
    public Result updateAccount(@RequestBody Account account) {
        accountService.updateAccount(account);
        return Result.success("编辑成功");
    }

//    @PutMapping("/update/{id}")
//    public Result updateAccount(@PathVariable int id, @RequestBody Account account) {
//        account.setId(id);
//        accountService.updateAccount(account);
//        return Result.success("编辑成功");
//    }

    @DeleteMapping("/delete/{id}")
    public Result deleteAccount(@PathVariable int id) {
        accountService.deleteAccount(id);
        return Result.success("删除成功");
    }



    // 批量删除
    @DeleteMapping("/deleteBatch")
    public Result deleteAccountBatch(@RequestBody List<Long> ids) {
        try {
            accountService.deleteAccountBatch(ids);
            return Result.success("批量删除成功");
        } catch (Exception e) {
            return Result.error("批量删除失败：" + e.getMessage());
        }
    }
}
