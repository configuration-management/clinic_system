/*
 Navicat Premium Data Transfer

 Source Server         : li
 Source Server Type    : MySQL
 Source Server Version : 80036 (8.0.36)
 Source Host           : localhost:3306
 Source Schema         : clinic_system

 Target Server Type    : MySQL
 Target Server Version : 80036 (8.0.36)
 File Encoding         : 65001

 Date: 30/05/2024 09:27:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for appointment
-- ----------------------------
DROP TABLE IF EXISTS `appointment`;
CREATE TABLE `appointment`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `patient_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `appointment_date` date NOT NULL,
  `appointment_time` time NOT NULL,
  `doctor_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `appointment_purpose` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'Pending',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of appointment
-- ----------------------------
INSERT INTO `appointment` VALUES (1, 'John Doe', '2024-04-12', '10:00:00', 'Dr. Smith', 'Regular checkup', '待治疗');
INSERT INTO `appointment` VALUES (2, 'Jane Smith', '2024-04-13', '11:30:00', 'Dr. Johnson', 'Dental cleaning', '已治疗');
INSERT INTO `appointment` VALUES (3, 'Michael Brown', '2024-04-14', '14:00:00', 'Dr. Lee', 'Toothache', '已治疗');
INSERT INTO `appointment` VALUES (11, '答案', '2024-05-20', '15:00:00', 'shai ', 'yateng', '已治疗');
INSERT INTO `appointment` VALUES (12, '阿萨', '2024-05-22', '16:00:00', 'saf', 'fsfs', '已治疗');
INSERT INTO `appointment` VALUES (16, '哇撒', '2024-05-23', '15:57:22', 'sacs', '阿三想啊', '已治疗');
INSERT INTO `appointment` VALUES (17, '飒飒', '2024-05-25', '14:05:35', '飒飒', '阿萨', '已治疗');
INSERT INTO `appointment` VALUES (18, '1111', '2024-05-30', '01:02:04', '2222', '222', '已治疗');
INSERT INTO `appointment` VALUES (19, 'zjj', '2024-05-22', '14:40:38', 'Dr.chen', '牙疼', '已治疗');

-- ----------------------------
-- Table structure for doctors
-- ----------------------------
DROP TABLE IF EXISTS `doctors`;
CREATE TABLE `doctors`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `doctor_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `schedule_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of doctors
-- ----------------------------
INSERT INTO `doctors` VALUES (1, 'Dr. Smith', '2024-05-22', '09:00:00', '17:00:00', 'Active');
INSERT INTO `doctors` VALUES (2, 'Dr. Johnson', '2024-05-22', '10:00:00', '18:00:00', 'Active');
INSERT INTO `doctors` VALUES (3, 'Dr. Williams', '2024-05-22', '08:00:00', '16:00:00', 'Inactive');
INSERT INTO `doctors` VALUES (4, 'Dr. Brown', '2024-05-22', '11:00:00', '19:00:00', 'Active');
INSERT INTO `doctors` VALUES (5, 'Dr. Jones', '2024-05-22', '07:00:00', '15:00:00', 'Inactive');
INSERT INTO `doctors` VALUES (6, 'Dr. Garcia', '2024-05-22', '12:00:00', '20:00:00', 'Active');
INSERT INTO `doctors` VALUES (7, 'Dr. Miller', '2024-05-22', '13:00:00', '21:00:00', 'Active');
INSERT INTO `doctors` VALUES (8, 'Dr. Davis', '2024-05-22', '06:00:00', '14:00:00', 'Inactive');
INSERT INTO `doctors` VALUES (9, 'Dr. Rodriguez', '2024-05-22', '14:00:00', '22:00:00', 'Active');
INSERT INTO `doctors` VALUES (10, 'Dr. Martinez', '2024-05-22', '15:00:00', '23:00:00', 'Inactive');
INSERT INTO `doctors` VALUES (14, 'Dr. Garcia', '2024-05-24', '12:00:00', '20:00:00', 'Active');

-- ----------------------------
-- Table structure for finances
-- ----------------------------
DROP TABLE IF EXISTS `finances`;
CREATE TABLE `finances`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(10, 2) NOT NULL,
  `date` date NOT NULL,
  `type` enum('收入','支出') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of finances
-- ----------------------------
INSERT INTO `finances` VALUES (1, '销售收入', 5000.00, '2024-01-01', '收入', '公司产品销售收入', '2024-05-21 09:21:40', '2024-05-21 09:21:40');
INSERT INTO `finances` VALUES (2, '办公用品采购', -150.50, '2024-01-02', '支出', '购买办公用品', '2024-05-21 09:21:40', '2024-05-21 09:21:40');
INSERT INTO `finances` VALUES (3, '员工工资', -3000.00, '2024-01-03', '支出', '支付员工工资', '2024-05-21 09:21:40', '2024-05-21 09:21:40');
INSERT INTO `finances` VALUES (4, '项目合同收入', 2000.00, '2024-01-04', '收入', '项目合同签订收入', '2024-05-21 09:21:40', '2024-05-21 09:21:40');
INSERT INTO `finances` VALUES (5, '租金支出', -1000.00, '2024-01-05', '支出', '支付办公室租金', '2024-05-21 09:21:40', '2024-05-21 09:21:40');
INSERT INTO `finances` VALUES (9, '治疗收入', 5.97, '2024-05-23', '支出', '洗牙[{\"号\":1,\"名\":\"牙膏\",\"数量\":1,\"金额\":2.99},{\"号\":2,\"名\":\"牙线\",\"数量\":2,\"金额\":2.98}]', '2024-05-23 08:46:26', '2024-05-26 02:39:42');
INSERT INTO `finances` VALUES (10, '库存采购', -100.00, '2024-05-23', '支出', '购买消毒水20瓶', '2024-05-23 08:48:22', '2024-05-24 16:54:40');
INSERT INTO `finances` VALUES (11, '治疗收入', 9.57, '2024-05-23', '收入', '洗牙[{\"号\":1,\"名\":\"牙膏\",\"数量\":1,\"金额\":2.99},{\"号\":3,\"名\":\"牙刷\",\"数量\":2,\"金额\":6.58}]', '2024-05-23 16:24:01', '2024-05-23 22:41:02');
INSERT INTO `finances` VALUES (12, '治疗收入', 100.00, '2024-05-24', '收入', '洗牙', '2024-05-24 17:31:58', '2024-05-24 17:31:58');
INSERT INTO `finances` VALUES (13, '治疗收入', 1000.00, '2024-05-22', '收入', '拔牙', '2024-05-24 17:32:30', '2024-05-26 01:47:51');
INSERT INTO `finances` VALUES (15, '洗牙', 9.28, '2024-05-08', '收入', '[{\"号\":4,\"名\":\"漱口水\",\"数量\":1,\"金额\":5.99},{\"号\":3,\"名\":\"牙刷\",\"数量\":1,\"金额\":3.29}]', '2024-05-28 20:54:32', '2024-05-28 20:54:32');
INSERT INTO `finances` VALUES (16, '[{}]', 2077.49, '2024-05-30', '收入', '[{\"号\":1,\"名\":\"牙线\",\"数量\":1,\"金额\":1.49}]', '2024-05-29 01:05:35', '2024-05-29 01:05:35');
INSERT INTO `finances` VALUES (17, '[{\"治疗方案\":\"牙齿修复\"},{\"治疗方案\":\"洗牙\"}]', 1307.49, '2024-05-09', '收入', '[{\"号\":1,\"名\":\"牙线\",\"数量\":1,\"金额\":1.49}]', '2024-05-29 01:08:29', '2024-05-29 01:08:29');
INSERT INTO `finances` VALUES (18, '[{\"治疗方案\":\"牙齿修复\"}]', 1191.29, '2024-05-16', '收入', '[{\"号\":2,\"名\":\"牙刷\",\"数量\":1,\"金额\":3.29}]', '2024-05-29 01:24:56', '2024-05-29 01:24:56');
INSERT INTO `finances` VALUES (19, '[{\"治疗方案\":\"牙齿美容\"},{\"治疗方案\":\"牙齿修复\"}]', 2092.99, '2024-04-30', '收入', '[{\"号\":4,\"名\":\"消毒水\",\"数量\":1,\"金额\":11},{\"号\":3,\"名\":\"漱口水\",\"数量\":1,\"金额\":5.99}]', '2024-05-29 01:27:20', '2024-05-29 01:27:20');
INSERT INTO `finances` VALUES (20, '[{\"治疗方案\":\"牙齿修复\"}]', 1189.49, '2024-05-08', '收入', '[{\"号\":1,\"名\":\"牙线\",\"数量\":1,\"金额\":1.49}]', '2024-05-29 01:30:16', '2024-05-29 01:30:16');
INSERT INTO `finances` VALUES (21, '[{\"治疗方案\":\"牙齿美容\"},{\"治疗方案\":\"洗牙\"}]', 1007.49, '2024-05-07', '收入', '[{\"号\":1,\"名\":\"牙线\",\"数量\":1,\"金额\":1.49}]', '2024-05-29 09:08:10', '2024-05-29 09:08:10');
INSERT INTO `finances` VALUES (22, '[{\"治疗方案\":\"牙齿修复\"}]', 1287.00, '2024-05-07', '收入', '[{\"号\":4,\"名\":\"消毒水\",\"数量\":9,\"金额\":99}]', '2024-05-29 09:13:22', '2024-05-29 09:13:22');
INSERT INTO `finances` VALUES (23, '[{\"治疗方案\":\"牙齿修复\",\"治疗金额\":\"1188.00\"}]', 1189.49, '2024-05-08', '收入', '[{\"号\":1,\"名\":\"牙线\",\"数量\":1,\"金额\":1.49}]', '2024-05-29 09:22:20', '2024-05-29 09:22:20');
INSERT INTO `finances` VALUES (24, '[{\"治疗方案\":\"牙齿修复\",\"治疗金额\":\"1188.00\"}]', 1204.99, '2024-05-07', '收入', '[{\"号\":3,\"名\":\"漱口水\",\"数量\":1,\"金额\":5.99},{\"号\":4,\"名\":\"消毒水\",\"数量\":1,\"金额\":11}]', '2024-05-29 09:39:08', '2024-05-29 09:39:08');
INSERT INTO `finances` VALUES (25, '[{\"治疗方案\":\"牙齿修复\",\"治疗金额\":\"1188.00\"},{\"治疗方案\":\"洗牙\",\"治疗金额\":\"118.00\"}]', 1307.49, '2024-05-10', '收入', '[{\"号\":1,\"名\":\"牙线\",\"数量\":1,\"金额\":1.49}]', '2024-05-29 09:48:19', '2024-05-29 09:48:19');
INSERT INTO `finances` VALUES (26, '[]', 3.33, '2024-05-29', '收入', '[{\"号\":6,\"名\":\"牙膏1\",\"数量\":1,\"金额\":3.33}]', '2024-05-29 21:29:56', '2024-05-29 21:29:56');
INSERT INTO `finances` VALUES (27, '[{\"治疗方案\":\"牙齿美容\",\"治疗金额\":\"888.00\"}]', 893.98, '2024-05-29', '收入', '[{\"号\":0,\"名\":\"牙膏\",\"数量\":2,\"金额\":5.98}]', '2024-05-29 22:51:35', '2024-05-29 22:51:35');
INSERT INTO `finances` VALUES (28, '[{\"治疗方案\":\"牙齿美容\",\"治疗金额\":\"888.00\"}]', 890.98, '2024-05-29', '收入', '[{\"号\":1,\"名\":\"牙线\",\"数量\":2,\"金额\":2.98}]', '2024-05-29 22:55:29', '2024-05-29 22:55:29');
INSERT INTO `finances` VALUES (29, 'a', 1.00, '2024-05-02', '支出', '', '2024-05-29 23:22:27', '2024-05-29 23:22:27');
INSERT INTO `finances` VALUES (30, '[{\"治疗方案\":\"牙齿美容\",\"治疗金额\":\"888.00\"},{\"治疗方案\":\"洗牙\",\"治疗金额\":\"118.00\"}]', 1014.98, '2024-05-07', '收入', '[{\"号\":0,\"名\":\"牙膏\",\"数量\":1,\"金额\":2.99},{\"号\":3,\"名\":\"漱口水\",\"数量\":1,\"金额\":5.99}]', '2024-05-30 00:24:14', '2024-05-30 00:24:14');

-- ----------------------------
-- Table structure for inventory
-- ----------------------------
DROP TABLE IF EXISTS `inventory`;
CREATE TABLE `inventory`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '序号',
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '产品名称',
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '类别',
  `brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '品牌',
  `supplier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '供应商',
  `specification` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '规格',
  `unit` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '单位',
  `quantity` int NULL DEFAULT NULL COMMENT '数量',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `product_batch` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '产品批次',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of inventory
-- ----------------------------
INSERT INTO `inventory` VALUES (1, '牙膏', '药品', '高露洁', 'ABC牙科用品', '100克', '盒', 66, 2.99, '20220401');
INSERT INTO `inventory` VALUES (2, '牙线', '药品', '佳洁士', 'XYZ牙科用品', '50米', '包', 68, 1.49, '20220402');
INSERT INTO `inventory` VALUES (3, '牙刷', '药品', '舒适达', 'DEF牙科用品', '软毛', '个', 7, 3.29, '20220403');
INSERT INTO `inventory` VALUES (4, '漱口水', '药品', '立克', 'GHI牙科用品', '500毫升', '瓶', 8, 5.99, '20220404');
INSERT INTO `inventory` VALUES (5, '消毒水', '药品', '舒适达', 'GHI牙科用品', '500毫升', '瓶', 9, 11.00, '20220405');
INSERT INTO `inventory` VALUES (6, '剪刀', '器械', '舒适达', 'GHI牙科用品', '小', '个', 49, 22.00, '20220406');
INSERT INTO `inventory` VALUES (32, '牙膏1', '药品', '高露洁', 'ABC牙科用品', '100克', '盒', 50, 3.33, '20220401');
INSERT INTO `inventory` VALUES (38, '牙膏1', '药品', '高露洁', 'ABC牙科用品', '100克', '盒', 50, 3.33, '20220401');
INSERT INTO `inventory` VALUES (39, '牙线1', '药品', '佳洁士', 'XYZ牙科用品', '50米', '包', 50, 4.44, '20220402');
INSERT INTO `inventory` VALUES (40, '牙膏1', '药品', '高露洁', 'ABC牙科用品', '100克', '盒', 50, 3.33, '20220401');
INSERT INTO `inventory` VALUES (41, '牙线1', '药品', '佳洁士', 'XYZ牙科用品', '50米', '包', 50, 4.44, '20220402');

-- ----------------------------
-- Table structure for patients
-- ----------------------------
DROP TABLE IF EXISTS `patients`;
CREATE TABLE `patients`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `gender` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 54 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of patients
-- ----------------------------
INSERT INTO `patients` VALUES (1, '张三', '男', '2000-05-29', '11111111', '11111111', '11111111');
INSERT INTO `patients` VALUES (41, '张三', '男', '1990-01-15', '1234567890', 'zhangsan@example.com', '北京市朝阳区');
INSERT INTO `patients` VALUES (42, '李四', '女', '1985-05-20', '0987654321', 'lisi@example.com', '上海市浦东新区');
INSERT INTO `patients` VALUES (43, '王五', '男', '1978-11-08', '1357924680', 'wangwu@example.com', '广州市天河区');
INSERT INTO `patients` VALUES (44, '赵六', '女', '1995-09-30', '2468013579', 'zhaoliu@example.com', '深圳市福田区');
INSERT INTO `patients` VALUES (45, '刘七', '男', '1982-03-25', '9876543210', 'liuqi@example.com', '成都市武侯区');
INSERT INTO `patients` VALUES (46, '孙八', '女', '1973-07-12', '6543210987', 'sunba@example.com', '重庆市渝中区');
INSERT INTO `patients` VALUES (47, '周九', '男', '1998-12-18', '1234567890', 'zhoujiu@example.com', '南京市鼓楼区');
INSERT INTO `patients` VALUES (48, '吴十', '女', '1992-06-02', '0987654321', 'wushi@example.com', '杭州市西湖区');
INSERT INTO `patients` VALUES (49, '郑十一', '男', '1980-10-09', '1357924680', 'zhengshiyi@example.com', '苏州市姑苏区');
INSERT INTO `patients` VALUES (50, '马十二', '女', '1987-04-28', '2468013579', 'mashier@example.com', '武汉市江汉区');

-- ----------------------------
-- Table structure for purchases
-- ----------------------------
DROP TABLE IF EXISTS `purchases`;
CREATE TABLE `purchases`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `specification` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int NOT NULL,
  `price` decimal(10, 2) NOT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdate` datetime NOT NULL,
  `purchasedate` datetime NULL DEFAULT NULL,
  `indate` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of purchases
-- ----------------------------
INSERT INTO `purchases` VALUES (1, '牙科工具A', '工具', '品牌A', '供应商A', '规格A', '件', 100, 50.00, '待采购', '2024-05-01 10:00:00', '2024-05-02 12:00:00', '2024-05-03 15:00:00');
INSERT INTO `purchases` VALUES (2, '牙科工具B', '工具', '品牌B', '供应商B', '规格B', '件', 200, 75.00, '已入库', '2024-05-05 10:00:00', '2024-05-06 12:00:00', '2024-05-07 15:00:00');
INSERT INTO `purchases` VALUES (3, '口腔药品A', '药品', '品牌C', '供应商C', '规格C', '瓶', 50, 150.00, '待入库', '2024-05-10 10:00:00', '2024-05-11 12:00:00', NULL);
INSERT INTO `purchases` VALUES (4, '口腔药品B', '药品', '品牌D', '供应商D', '规格D', '瓶', 75, 200.00, '已入库', '2024-05-15 10:00:00', '2024-05-16 12:00:00', '2024-05-17 15:00:00');
INSERT INTO `purchases` VALUES (5, '牙科设备A', '设备', '品牌E', '供应商E', '规格E', '台', 10, 5000.00, '待入库', '2024-05-20 10:00:00', '2024-05-21 12:00:00', NULL);

-- ----------------------------
-- Table structure for treatment
-- ----------------------------
DROP TABLE IF EXISTS `treatment`;
CREATE TABLE `treatment`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `patient_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `appointment_purpose` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `doctor_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `treatment_date` date NOT NULL,
  `treatment_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `treatment_product` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `treatment_fee` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of treatment
-- ----------------------------
INSERT INTO `treatment` VALUES (1, '张三', '11', '已治疗', '李医生', '2024-05-16', '牙齿清洁', NULL, '10');
INSERT INTO `treatment` VALUES (2, '李四', '11', '已治疗', '王医生', '2024-05-17', '补牙', NULL, '10');
INSERT INTO `treatment` VALUES (3, '王五', '11', '已治疗', '刘医生', '2024-05-18', '牙齿美白', NULL, '10');
INSERT INTO `treatment` VALUES (4, '赵六', '11', '已治疗', '陈医生', '2024-05-19', '牙齿矫正', NULL, '10');
INSERT INTO `treatment` VALUES (5, '孙七', '11', '已治疗', '周医生', '2024-05-20', '拔牙', NULL, '10');
INSERT INTO `treatment` VALUES (6, '答案', 'yateng', '已治疗', 'shai ', '2024-05-21', '消炎', NULL, '20');
INSERT INTO `treatment` VALUES (7, '撒v', 'das', '已治疗', 'asdsa', '1970-01-01', '压缩', NULL, '100');
INSERT INTO `treatment` VALUES (8, '阿萨', 'fsfs', '已治疗', 'saf', '2024-05-22', '杀杀杀', NULL, '1000');
INSERT INTO `treatment` VALUES (9, 'sa', 'as', '已治疗', 'sa', '1970-01-01', '洗牙', NULL, '8.98');
INSERT INTO `treatment` VALUES (31, '撒v', 'das', '已治疗', 'asdsa', '2024-05-23', '洗牙', NULL, '5.970000000000001');
INSERT INTO `treatment` VALUES (32, '哇撒', '阿三想啊', '已治疗', 'sacs', '2024-05-23', '洗牙', NULL, '9.57');
INSERT INTO `treatment` VALUES (33, '飒飒', '阿萨', '已治疗', '飒飒', '2024-05-08', '洗牙', NULL, '9.28');
INSERT INTO `treatment` VALUES (40, '撒v', 'das', '已治疗', 'asdsa', '2024-05-07', '[{\"治疗方案\":\"牙齿美容\"},{\"治疗方案\":\"洗牙\"}]', NULL, '1007.49');
INSERT INTO `treatment` VALUES (41, '撒v', 'das', '已治疗', 'asdsa', '2024-05-07', '[{\"治疗方案\":\"牙齿修复\"}]', NULL, '1287.00');
INSERT INTO `treatment` VALUES (42, '撒v', 'das', '已治疗', 'asdsa', '2024-05-08', '[{\"治疗方案\":\"牙齿修复\",\"治疗金额\":\"1188.00\"}]', NULL, '1189.49');
INSERT INTO `treatment` VALUES (43, '撒v', 'das', '已治疗', 'asdsa', '2024-05-07', '[{\"治疗方案\":\"牙齿修复\",\"治疗金额\":\"1188.00\"}]', '[{\"治疗用具名称\":\"漱口水\",\"治疗用具数量\":1,\"治疗用具金额\":5.99},{\"治疗用具名称\":\"消毒水\",\"治疗用具数量\":1,\"治疗用具金额\":11}]', '1204.99');
INSERT INTO `treatment` VALUES (44, '撒v', 'das', '已治疗', 'asdsa', '2024-05-10', '[{\"治疗方案\":\"牙齿修复\",\"治疗金额\":\"1188.00\"},{\"治疗方案\":\"洗牙\",\"治疗金额\":\"118.00\"}]', '[{\"治疗用具名称\":\"牙线\",\"治疗用具数量\":1,\"治疗用具金额\":1.49}]', '1307.49');
INSERT INTO `treatment` VALUES (45, 'John Doe', 'Regular checkup', '已治疗', 'Dr. Smith', '2024-05-29', '[]', '[{\"治疗用具名称\":\"牙膏1\",\"治疗用具数量\":1,\"治疗用具金额\":3.33}]', '3.33');
INSERT INTO `treatment` VALUES (46, 'Jane Smith', 'Dental cleaning', '已治疗', 'Dr. Johnson', '2024-05-29', '[{\"治疗方案\":\"牙齿美容\",\"治疗金额\":\"888.00\"}]', '[{\"治疗用具名称\":\"牙膏\",\"治疗用具数量\":2,\"治疗用具金额\":5.98}]', '893.98');
INSERT INTO `treatment` VALUES (47, 'zjj', '牙疼', '已治疗', 'Dr.chen', '2024-05-29', '[{\"治疗方案\":\"牙齿美容\",\"治疗金额\":\"888.00\"}]', '[{\"治疗用具名称\":\"牙线\",\"治疗用具数量\":2,\"治疗用具金额\":2.98}]', '890.98');
INSERT INTO `treatment` VALUES (48, 'Michael Brown', 'Toothache', '已治疗', 'Dr. Lee', '2024-05-07', '[{\"治疗方案\":\"牙齿美容\",\"治疗金额\":\"888.00\"},{\"治疗方案\":\"洗牙\",\"治疗金额\":\"118.00\"}]', '[{\"治疗用具名称\":\"牙膏\",\"治疗用具数量\":1,\"治疗用具金额\":2.99},{\"治疗用具名称\":\"漱口水\",\"治疗用具数量\":1,\"治疗用具金额\":5.99}]', '1014.98');

-- ----------------------------
-- Table structure for treatment_plans
-- ----------------------------
DROP TABLE IF EXISTS `treatment_plans`;
CREATE TABLE `treatment_plans`  (
  `id` int NOT NULL,
  `treatment_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `treatment_free` decimal(10, 2) NOT NULL
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of treatment_plans
-- ----------------------------
INSERT INTO `treatment_plans` VALUES (101, '牙齿美容', 888.00);
INSERT INTO `treatment_plans` VALUES (102, '牙齿修复', 1188.00);
INSERT INTO `treatment_plans` VALUES (103, '拔牙', 68.00);
INSERT INTO `treatment_plans` VALUES (104, '洗牙', 118.00);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名字',
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', '1234', '管理员', '管理员');
INSERT INTO `users` VALUES (2, 'doctorz', '1234', '张三', '医生');
INSERT INTO `users` VALUES (3, 'doctorl', '1234', '李四', '医生');
INSERT INTO `users` VALUES (4, 'nursez', '1234', '张美', '护士');

SET FOREIGN_KEY_CHECKS = 1;
