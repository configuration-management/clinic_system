package com.example.springboot.service;

import com.example.springboot.entity.User;
import com.example.springboot.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
//    public User findByUsername(String username){
//        return userMapper.findByUsername(username);
//    }
    public List<User> findByUsername(String username) {
        return userMapper.findByUsername(username);
    }
}
