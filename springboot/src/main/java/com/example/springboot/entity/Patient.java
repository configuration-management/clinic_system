package com.example.springboot.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Patient {
    private int id;  //序号
    private String name;  //患者姓名
    private String gender;      //患者性别
    private Date date_of_birth;     //出生日期
    private String phone;         //手机号码
    private String email;      //邮箱
    private String address; //地址
}
