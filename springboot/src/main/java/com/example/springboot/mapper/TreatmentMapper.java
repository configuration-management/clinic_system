package com.example.springboot.mapper;


import com.example.springboot.entity.Treatment;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface TreatmentMapper {

    @Select("select * from treatment ")
    List<Treatment> selectAll();

//    @Select("SELECT * FROM treatment WHERE patient_name LIKE CONCAT('%', #{patientName}, '%')")
//    List<Treatment> searchTreatmentsByPatientName(@Param("patientName") String patientName);
    @Select("select * from treatment where id = #{id}")
    List<Treatment> selectById(@Param("id") Long id);

    @Select("select * from treatment where product_name = #{name}")
    List<Treatment> selectByName(String name);

    @Insert("INSERT INTO treatment (patient_name, appointment_purpose, status, doctor_name, treatment_date, treatment_content, treatment_product,treatment_fee) " +
            "VALUES (#{patient_name}, #{appointment_purpose}, #{status}, #{doctor_name}, #{treatment_date}, #{treatment_content}, #{treatment_product},#{treatment_fee})")
    void addTreatment(Treatment treatment);

    @Update("UPDATE treatment SET patient_name = #{patient_name}, treatment_date = #{treatment_date},  " +
            "treatment_details = #{treatment_details}, doctor_name = #{doctor_name} WHERE id = #{id}")
    void editTreatment(Treatment treatment);

    @Delete("DELETE FROM treatment WHERE id = #{id}")
    void deleteTreatment(@Param("id") Long id);
}
