package com.example.springboot.entity;

import lombok.Data;

@Data
public class Treatment_plans {

    private int id;
    private String  treatment_content;
    private String  treatment_free;

}
