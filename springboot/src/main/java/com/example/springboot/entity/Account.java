package com.example.springboot.entity;

import lombok.Data;
@Data
public class Account {
    private int id;
    private String username; // 账号名称
    private String password; // 密码
    private String name;
    private String role;     // 角色（管理员、医生、护士等）
}
