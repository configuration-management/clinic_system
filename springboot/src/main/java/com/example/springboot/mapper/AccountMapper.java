package com.example.springboot.mapper;

import com.example.springboot.entity.Account;
import com.example.springboot.entity.Appointment;
import com.example.springboot.entity.Treatment;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AccountMapper {

//    @Select("<script>" +
//            "SELECT * FROM accounts" +
//            "<if test='username != null and username != \"\"'>" +
//            "WHERE username LIKE CONCAT('%', #{username}, '%')" +
//            "</if>" +
//            "</script>")
//    List<Account> searchAccounts(@Param("username") String username);
    @Select("select * from users ")
    List<Account> searchAccounts();

    @Select("select * from users where id = #{id}")
    List<Account> selectById(Long id);

    @Insert("INSERT INTO users (username, password,name, role) VALUES (#{username}, #{password},#{name}, #{role})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void addAccount(Account account);

    @Update("UPDATE users SET username = #{username}, password = #{password}, role = #{role},name=#{name} WHERE id = #{id}")
    void updateAccount(Account account);

    @Delete("DELETE FROM users WHERE id = #{id}")
    void deleteAccount(@Param("id") int id);

    @Delete({
            "<script>",
            "DELETE FROM users WHERE id IN ",
            "<foreach item='id' collection='list' open='(' separator=',' close=')'>#{id}</foreach>",
            "</script>"
    })
    void deleteAccountBatch(List<Long> ids);

    @Select("select * from users where name = #{name}")
    List<Account> selectByName(String name);
}
