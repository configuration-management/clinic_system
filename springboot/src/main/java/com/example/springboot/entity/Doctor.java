package com.example.springboot.entity;

import lombok.Data;
import java.sql.Date;
import java.sql.Time;

@Data
public class Doctor{
    private Long id;
    private String doctor_name;         // 医生姓名
    private Date schedule_date;       // 排班日期
    private Time start_time;         // 开始时间
    private Time end_time;           // 结束时间
    private String status;         // 状态，如"忙碌", "空闲"
}