package com.example.springboot.entity;

import lombok.Data;

import java.sql.Date;
import java.sql.Time;

@Data
public class Treatment {
    private Long id;
    private String patient_name;        // 患者姓名
    private String appointment_purpose; // 预约目的
    private String status;              //状态
    private String doctor_name;         // 医生姓名
    private Date treatment_date;        // 治疗日期
    private String treatment_content;   // 治疗详情
    private String treatment_product;   // 治疗用具
    private String treatment_fee;       // 治疗金额

}
