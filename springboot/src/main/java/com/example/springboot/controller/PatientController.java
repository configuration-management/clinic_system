package com.example.springboot.controller;


import com.example.springboot.common.Result;
import com.example.springboot.entity.Patient;
import com.example.springboot.service.PatientService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:7070")
@RestController
@RequestMapping("/patients")
public class PatientController {

    @Autowired
    private PatientService patientService;

    @GetMapping("/selectAll")
    public Result getPatientList(@RequestParam int page, @RequestParam int size) {
        PageHelper.startPage(page, size);
        List<Patient> PatientList = patientService.selectAll();
        PageInfo<Patient> pageInfo = new PageInfo<>(PatientList);
        return Result.success(pageInfo);
    }

    @GetMapping("/selectByid")
    public Result selectById(@RequestParam Long id,@RequestParam int page, @RequestParam int size) {
        PageHelper.startPage(page, size);
        List<Patient> PatientList = patientService.selectById(id);
        PageInfo<Patient> pageInfo = new PageInfo<>(PatientList);
        return Result.success(pageInfo);
    }

    @GetMapping("/selectByname")
    public Result selectByName(@RequestParam String name,@RequestParam int page, @RequestParam int size) {
        PageHelper.startPage(page, size);
        List<Patient> accounts = patientService.selectByName(name);
        PageInfo<Patient> pageInfo = new PageInfo<>(accounts);
        return Result.success(pageInfo);
    }

    @PostMapping("/add")
    public Result addPatient(@RequestBody Patient patient) {
        patientService.addPatient(patient);
        return Result.success("新增成功");
    }

    @PutMapping("/edit")
    public Result updatePatient(@RequestBody Patient patient) {
        patientService.updatePatient(patient);
        return Result.success("编辑成功");
    }

    @DeleteMapping("/delete/{id}")
    public Result deletePatient(@PathVariable int id) {
        patientService.deletePatient(id);
        return Result.success("删除成功");
    }

    // 批量删除
    @DeleteMapping("/deleteBatch")
    public Result deletePatientBatch(@RequestBody List<Long> ids) {
        try {
            patientService.deletePatientBatch(ids);
            return Result.success("批量删除成功");
        } catch (Exception e) {
            return Result.error("批量删除失败：" + e.getMessage());
        }
    }

}
