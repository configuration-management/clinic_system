package com.example.springboot.entity;

import lombok.Data;


@Data
public class User {
    private int id;              //ID
    private String username;     //用户名
    private String password;     //密码
    private String name;         //名字
    private String role;
}
