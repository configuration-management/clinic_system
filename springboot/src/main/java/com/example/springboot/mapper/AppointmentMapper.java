package com.example.springboot.mapper;

import com.example.springboot.entity.Appointment;
import com.example.springboot.entity.Inventory;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AppointmentMapper {

    @Select("SELECT * FROM appointment")
    List<Appointment> selectAll();

    @Select("SELECT * FROM appointment where status = #{status}")
    List<Appointment> findAllByStatus(String status);


    @Select("select * from appointment where id = #{id}")
    List<Appointment> selectById(@Param("id") Long id);

    @Select("select * from appointment where id = #{id} and status = #{status}")
    List<Appointment> findByIdAndStatus(Long id, String status);


    @Select("select * from appointment where product_name = #{name}")
    List<Appointment> selectByName(String name);

    @Select("select * from appointment where product_name = #{name} and status = #{status}")
    List<Appointment> findByNameAndStatus(String name, String status);


    @Insert("INSERT INTO appointment (patient_name, appointment_date, appointment_time, doctor_name, appointment_purpose, status) " +
            "VALUES (#{patient_name}, #{appointment_date}, #{appointment_time}, #{doctor_name}, #{appointment_purpose}, #{status})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(Appointment appointment);

    @Update("UPDATE appointment SET status = #{status} WHERE id = #{id}")
    void updateStatus(@Param("id") Long id, @Param("status") String status);

    @Update("UPDATE appointment SET patient_name = #{patient_name}, appointment_date = #{appointment_date}, appointment_time = #{appointment_time}, " +
            "doctor_name = #{doctor_name}, appointment_purpose = #{appointment_purpose}, status = #{status} WHERE id = #{id}")
    void update(Appointment appointment);

    @Delete("DELETE FROM appointment WHERE id = #{id}")
    void delete(@Param("id") int id);

    @Delete({
            "<script>",
            "DELETE FROM appointment WHERE id IN ",
            "<foreach item='id' collection='list' open='(' separator=',' close=')'>#{id}</foreach>",
            "</script>"
    })
    void deleteAppointmentBatch(@Param("list") List<Long> ids);

}
