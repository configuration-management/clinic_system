package com.example.springboot.service;

import com.example.springboot.entity.Appointment;
import com.example.springboot.mapper.AppointmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AppointmentService {

    @Autowired
    private AppointmentMapper appointmentMapper;

    public List<Appointment> getAllAppointments() {
        return appointmentMapper.selectAll();
    }
    public List<Appointment> getAllAppointmentsByStatus(String status) {
        return appointmentMapper.findAllByStatus(status);
    }

    public List<Appointment> selectById(Long id) {
        return appointmentMapper.selectById(id);
    }
    public List<Appointment> selectByIdAndStatus(Long id, String status) {
        return appointmentMapper.findByIdAndStatus(id, status);
    }

    public List<Appointment> selectByName(String name) {
        return appointmentMapper.selectByName(name);
    }
    public List<Appointment> selectByNameAndStatus(String name, String status) {
        return appointmentMapper.findByNameAndStatus(name, status);
    }

//    public Appointment getAppointmentById(long id) {
//        return appointmentMapper.selectById(id);
//    }

    public void addAppointment(Appointment appointment) {
        appointmentMapper.insert(appointment);
    }

    public void editAppointment(Appointment appointment) {
        appointmentMapper.update(appointment);
    }

    public void deleteAppointment(int id) {
        appointmentMapper.delete(id);
    }

    @Transactional
    public void deleteAppointmentBatch(List<Long> ids) {
        // 根据传入的 ID 列表批量删除对应的库存信息
        appointmentMapper.deleteAppointmentBatch(ids);
    }

    public void updateStatus(Long id, String status) {
        appointmentMapper.updateStatus(id, status);
    }
}
