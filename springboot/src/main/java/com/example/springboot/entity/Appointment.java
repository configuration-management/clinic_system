package com.example.springboot.entity;

import lombok.Data;

import java.sql.Date;
import java.sql.Time;

@Data
public class Appointment {
    private int id;
    private String patient_name;        // 预约人姓名
    private Date appointment_date; // 预约日期
    private Time appointment_time; // 预约时间
    private String doctor_name; // 预约人医生
    private String appointment_purpose;// 预约目的
    private String status;     // 状态
}
