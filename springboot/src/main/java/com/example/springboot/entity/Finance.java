package com.example.springboot.entity;

import lombok.Data;

@Data
public class Finance {
    private int id;
    private String name;
    private double amount;
    private String date;
    private String type;
    private String remark;
}
