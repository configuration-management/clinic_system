package com.example.springboot.service;

import com.example.springboot.entity.Account;
import com.example.springboot.entity.Patient;

import com.example.springboot.mapper.PatientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientService {

    @Autowired
    private PatientMapper patientMapper;

    public List<Patient> selectAll() {
        return patientMapper.selectAll();
    }

    public List<Patient> selectById(Long id) {
        return patientMapper.selectById(id);
    }

    public List<Patient> selectByName(String name) {
        return patientMapper.selectByName(name);
    }

    public void addPatient(Patient patient) {
        patientMapper.addPatient(patient);
    }

    public void updatePatient(Patient patient) {
        patientMapper.updatePatient(patient);
    }

    public void deletePatient(int id) {
        patientMapper.deletePatient(id);
    }

    public void deletePatientBatch(List<Long> ids) {
        patientMapper.deletePatientBatch(ids);
    }
}
