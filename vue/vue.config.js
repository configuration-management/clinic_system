const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  publicPath: './',//公共路径
  transpileDependencies: true,
  devServer: {
    port: 7070 // 将此处修改为您想要的端口号
  }
})
